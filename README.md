# Workstation Setup

Playbook to configure a new workstation.

## Testing

To test the custom roles:

```shell
cd roles/<role_name>
molecule test
```

This requires a version of `molecule` installed with `pip3`.

## Installation

To run the playbook on your local machine:

```shell
install.sh
```
