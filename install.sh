#!/bin/bash

run () {
    echo "+" "$@" 1>&2
    "$@"
}

usage () {
    cat <<EOF
Usage: $0

  Runs the Ansible configuration in "./playbook.yml" in Ansible local mode to
  install it on the current machine.

EOF
}

if [[ $# -ne 0 ]]; then
    usage
    exit 1
fi

echo "Initializing submodules..."
git submodule init
git submodule sync

echo "Checking if ansible is installed..."
if ! command -v ansible &> /dev/null; then
    echo "Error: ansible not installed!  Install ansible first."
    exit 1
fi

echo "Running sudo before ansible in case we have a password prompt..."
sudo test

echo "Running the ansible playbook in local mode..."
run ansible-playbook -i "localhost," -c local playbook.yml
